#!/usr/bin/python
from MDSplus import *
#import os
import sys

import matplotlib.pyplot as plt
#import numpy as np
import pymysql


mdsTreeName = 'esther_mds'
mdsPulseNumber = int(sys.argv[1])

cnx = pymysql.connect(user='report', password='$report',
                             host='localhost',
                             cursorclass=pymysql.cursors.DictCursor,
                             database='archive')

try:
   cursor = cnx.cursor()
   sql = "SELECT `range_kistler`,`delta_P_kistler` from `esther_reports` where `shot_number` = %s"

   cursor.execute(sql,(mdsPulseNumber,))
   result = cursor.fetchone()
   print "Range {0}, delta P: {1}".format(result['range_kistler'], result['delta_P_kistler'])
#
#   + result['range_kistler'] + ", delta P: " + result['delta_P_kistler']
#   print "Range " + result['range_kistler'] + ", delta P: " + result['delta_P_kistler']
finally:
    cnx.close()

try:
    tree = Tree(mdsTreeName, mdsPulseNumber)
except:
    print 'Failed opening ' + mdsTreeName + ' for pulse number ' + str(mdsPulseNumber)
    exit()

#segment=0
#t = Tree("esther_mds", shot_no)
nd =tree.getNode("KISTLER.RANGE")
gData = nd.getData()
print "Range: ", gData, " Bar"
#KISTLER Pressure Data
# Use FULL instead of DECIMATED to get full acquisition data
signalName1 = "KISTLER.SIG.DECIMATED"
signalName2 = "WIRE.SIG.DECIMATED"
try:
    node1 = tree.getNode(signalName1)
    node2 = tree.getNode(signalName2)
except:
    print 'No data available for pulse number ' + str(mdsPulseNumber)
    exit()

#segData = nd.getSegment(segment)
kistData = node1.getData().data()
timeData = node1.getDimensionAt(0).data()

plt.subplot(2, 1, 1)
plt.plot(timeData,kistData , 'r')
plt.ylabel('Pressure (Bar)')
wireData = node2.getData().data()
timeData = node2.getDimensionAt(0).data()
plt.subplot(2, 1, 2)
plt.plot(timeData, wireData, 'g')
plt.ylabel('Wire/Laser')
plt.xlabel('Time / s')

plt.show()


